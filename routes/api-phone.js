const   express   =   require('express')
const   router  =   express.Router()
const   Vehicle =   require('../models/vehiclerc')
const   Log =   require('../models/log')
const   fs =   require('fs')
const   fileUpload  =   require('express-fileupload')
const   Emission    =   require('../models/emission')
const Fine=require('../models/fines')
const Insurance=require('../models/insurance')
const rn=require('random-number')
const Police=require('../models/police');

var options={
    min: 100000,
    max: 999999,
    integer:true
}

router.get('/show-documents/:registration_number', (req,res,next)=>{
    registration_number=req.params.registration_number
    username=req.body.username
    vehicleDocuments=({
        registration_number:registration_number,
        insurance_status: true,
        emission_status:true,
    })
    Vehicle.find({registration_number:registration_number},(err, vehicle)=>{
        if(err){

            throw err
        }
        if(vehicle){
            Insurance.find({registration_number:registration_number}, (err, insurance)=>{
                Emission.find({registration_number:registration_number},(err, emission)=>{
                    if(err){
                     throw err   
                    }
                    if(insurance[0].end_date<Date.now()){
                            fineid=rn(options)
                            let newFine=new Fine({
                                registration_number:registration_number,
                                fine_id:fineid,
                                offense:"Insurance Documents lapsed",
                                fine_date:new Date,
                                fine_amount: 500,
                                fine_status: "Pending",
                                police_username: username
                            });
                            Fine.addFine(newFine);
                            vehicleDocuments.insurance_status=false
                        }
                    if(emission[0].end_date<Date.now()){
                        fineid=rn(options)
                        let newFine=new Fine({
                            registration_number:registration_number,
                            fine_id:fineid,
                            offense:"Emission Test Documents lapsed",
                            fine_date:new Date,
                            fine_amount: 300,
                            fine_status: "Pending",
                            police_username: username
                        });
                        Fine.addFine(newFine);
                        vehicleDocuments.emission_status=false
                    }
               
                    res.json(vehicleDocuments)
                    });
                    });
                }
            });
        });

router.get('/show-emission/:registration_number',(req,res,next)=>{
    registration_number=req.params.registration_number
    Emission.find({registration_number: registration_number}, (err,emission)=>{
        if(err){
            throw err
        }
        else{
            res.json(emission)
        }
    })
            if(emission.end_date>Date.now()){
               
                Fine.addFine(newFine, (err)=>{
                    if(err){
                        throw err
                    }
                    else{
                        res.json({success:true, msg})
                    }
                })
            }
        })

router.get('/show-vehiclerc/:registration_number',(req,res,next)=>{
    Vehicle.find({registration_number:registration_number},(err,vehicle)=>{
        if(err){
            throw err;
        }
        else{
            res.json(vehicle);
        }
    })
});

router.get('/show-insurance/:registration_number',(req,res,next)=>{
    Insurance.find({registration_number:registration_number},(err,insurance)=>{
        if(err){
            throw err
        }
        else{
            res.json(insurance)
        }
    });
});

router.post('/login-police', (req,res,next)=>{
    police_name=req.body.police_name
    police_id=req.body.police_id
    Police.findOne({police_name:police_name},(err,police)=>{
        if(err){
            throw err
        }
        if(!police){
            res.json({success:false, msg:"Invalid Police Name"})
        }
        else{
            Police.checkPolice(police_id, police.police_id, (err,isMatch)=>{
                if(err){
                    throw err
                }
                if(isMatch){
                    res.json({success:true, msg:"Login successful"})
                }
                else{
                    res.json({success:false, msg:"Invalid Police Id"})
                }
            })

        }
    })
})




module.exports=router;


