const   express   =   require('express');
const   router  =   express.Router();
const   Vehicle =   require('../models/vehiclerc');
const   Log =   require('../models/log');
const   fs =   require('fs');
const   fileUpload  =   require('express-fileupload');
const   Emission    =   require('../models/emission');
const Insurance =   require('../models/insurance')
const Police    =   require('../models/police')


router.get('/police-register', (req,res,next)=>{
    res.render('police-register')
})

router.post('/police-register', (req,res,next)=>{
    police_id=req.body.police_id
    Police.addPolice(police_id, (err, police)=>{
        if(err){
            res.json({success:false, msg:"Error occured"})
        }
        else{
            res.json({success:false, msg:"Police Registered Successfully"})
        }
    })
})
router.get('/add-vehiclerc', (req,res,next)=>{
    res.render('add-vehiclerc')
})
router.post('/add-vehiclerc',(req,res,next)=>{
    let newVehiclerc  =   new Vehicle({
        registration_number :   req.body.registration_number,
        registration_date   :   new Date,
        owner_name  :   req.body.owner_name,
        chassis_number  :   req.body.chassis_number,
        vehicle_model   :   req.body.vehicle_model,
        fuel_type   :   req.body.fuel_type
    });
    Vehicle.findOne({registration_number:newVehiclerc.registration_number},(err,vehicle)=>{
        if(err){
            throw err;
        }
        if(vehicle){
            res.json({success:false,msg:"registration number already exist"});
        }
        else{
            Vehicle.addVehiclerc(newVehiclerc,(err,vehicle)=>{
                if(err){
                    throw err;
                }
                else{
                    newLog  =   new Log({
                        log_activity    :   "new vehicle RC added",
                        log_date    :   new Date,
                        log_registration_number :   newVehiclerc.registration_number   
                    });
                    Log.addLog(newLog,(err,log)=>{
                        if(err){
                            res.json({success:false,msg:"logging failed"});
                        }
                        else{
                            res.json({success:true,msg:"registered successfully"});
                        }
                    });
                }
            });
        }
    });
});

// router.post('/update-vehiclerc/:registration_number',(req,res,next)=>{
//     username    =   req.body.username;
//     let updateVehiclerc =({
//     registration_number :   req.params.registration_number,
//     registration_date   :   new Date,
//     owner_name  :   req.body.owner_name,
//     chassis_number  : req.body.chassis_number,
//     vehicle_model   :   req.body.vehicle_model ,
//     fuel_type: req.body.fuel_type
//     });
//     Vehicle.updateVehiclerc(updateVehiclerc.registration_number,updateVehiclerc,(err)=>{
//         if(err){
//             throw err;
//         }
//         else{
//             newLog  =   new Log({
//                 log_activity    :   "new vehicle RC updated",
//                 log_date    :   new Date,
//                 log_username    :   username,
//                 log_registration_number :   updateVehiclerc.registration_number 
//             });
//             Log.addLog(newLog,(err,log)=>{
//                 if(err){
//                     throw err;
//                 }
//                 else{
//                     res.json({success:true,msg:"updated successfully"});
//                 }
//             });
//         }
//     });
// });


router.get('/add-emission',(req,res,next)=>{
    res.render('add-emission')
})
router.post('/add-emission',(req,res,next)=>{
    let img_file    =   req.files.number_plate_picture
    registration_number=req.body.registration_number
    Emission.findOne({registration_number:registration_number},(err,emission)=>{
     if(err){
         throw err;
     }
     if(emission){
         res.json({success:false,msg:"already exist"})
     }
     else{
    Vehicle.findOne({registration_number:registration_number},(err,vehicle)=>{
        if(err){
            throw err;
        }
        if(vehicle){
    img_file.mv('./public/images/photo_'+registration_number+'.jpg', (err)=>{
        if(err){
            throw err;
        }
        else{
            console.log('Success');
        }
    });
    newEmission =   new Emission({
    registration_number :   req.body.registration_number,
    number_plate_picture:'http://localhost:3000/images/photo_'+registration_number+'.jpg',
    start_date:new Date,
    end_date: req.body.end_date
    });
    Emission.addEmissionDetails(newEmission, (err, emission)=>{
        if(err){
            throw err;
        }
        else{
            newLog  =   new Log({
                log_activity    :   "new vehicle emision added",
                log_date    :   new Date,
                log_registration_number :   newEmission.registration_number 
            });
            Log.addLog(newLog,(err,log)=>{
                if(err){
                    throw err;
                }
                else{
                    res.json({success:true,msg:"emission added successfully"});
                }
            });
        }
    });
    }
   else{
    res.json({success:false,msg:"registration number not found"});
   } 
  });
    }
    });
});

// router.post('/update-emission/:registration_number',(req,res,next)=>{
//     let img_file    =   req.files.number_plate_picture
//     registration_number=req.params.registration_number
//     Vehicle.find({registration_number:registration_number},(err,vehicle)=>{
//         if(err){
//             throw err;
//         }
//         if(vehicle){
//     img_file.mv('./public/images/photo_'+registration_number+'.jpg', (err)=>{
//         if(err){
//             throw err;
//         }
//         else{
//             console.log('Success');
//         }
//     });
//     newEmission =  ({
//     registration_number :   req.body.registration_number,
//     number_plate_picture:'http://localhost:3000/images/photo_'+registration_number+'.jpg',
//     start_date:req.body.start_date,
//     end_date:req.body.end_date 
//     });
//     Emission.updateEmissionDetails(newEmission, (err, emission)=>{
//         if(err){
//             throw err;
//         }
//         else{
//             newLog  =   new Log({
//                 log_activity    :   "Emission updated",
//                 log_date    :   new Date,
//                 log_username    :   username,
//                 log_registration_number :   newEmission.registration_number 
//             });
//             Log.addLog(newLog,(err,log)=>{
//                 if(err){
//                     throw err;
//                 }
//                 else{
//                     res.json({success:true,msg:"emission updated successfully"});
//                 }
//             });
//         }
//     });
//     }
//     else{
//     res.json({success:false,msg:"registration number not found"});
//      }
//     });
// });
router.get('/add-insurance', (req,res,next)=>{
    res.render('add-insurance')
})
router.post('/add-insurance',(req,res,next)=>{
    let newInsurance    =   new Insurance({
    registration_number :   req.body.registration_number,
    insurance_number    :   req.body.insurance_number,
    owner_name  :   req.body.owner_name,
    from_date   :   new Date,
    end_date    :   req.body.end_date,
    insurance_name  :   req.body.insurance_name
    });
    Insurance.findOne({registration_number:newInsurance.registration_number},(err,insurance)=>{
        if(err){
            throw err
        }
        if(insurance){
            res.json({success:false, msg:"Already exists"})
        }
        else{
            Vehicle.find({registration_number:newInsurance.registration_number}, (err, vehicle)=>{
                if(err){
                    throw err
                }
                if(!vehicle){
                    res.json({success:false, msg:"No vehicle by this registration"})
                }
                else{
                    Insurance.addInsuranceDetails(newInsurance, (err, insurance)=>{
                        if(err){
                            throw err;
                        }
                        else{
                            newLog  =   new Log({
                                log_activity    :   "new vehicle insurance added",
                                log_date    :   new Date,
                                log_registration_number :   newInsurance.registration_number   
                            });
                            Log.addLog(newLog,(err,log)=>{
                                if(err){
                                    res.json({success:false,msg:"logging failed"});
                                }
                                else{
                                    res.json({success:true,msg:"registered successfully"});
                                }
                            });
                        }
                    })
                }
            });
        }
    });

});

// router.post('/update-insurance/:registration_number', (req,res,next)=>{
//     let newInsurance    =   ({
//         registration_number :   req.params.registration_number,
//         insurance_number    :   req.body.insurance_number,
//         owner_name  :   req.body.owner_name,
//         from_date   :   req.body.from_date,
//         end_date    :   req.body.end_date,
//         insurance_name  :   req.body.insurance_name
//         });
//         username    =   req.body.username;
//         Insurance.updateInsurance(newInsurance, (err, insurance)=>{
//             if(err){
//                 throw err;
//             }
//             else{
//                 newLog  = ({
//                     log_activity    :   "Insurance updated",
//                     log_date    :   new Date,
//                     log_username    :   username,
//                     log_registration_number :   newInsurance.registration_number 
//                 });
//                 Log.addLog(newLog,(err,log)=>{
//                     if(err){
//                         throw err;
//                     }
//                     else{
//                         res.json({success:true,msg:"Insurance updated successfully"});
//                     }
//                 });
//             }
//         });
// });


module.exports=router;