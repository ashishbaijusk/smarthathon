const mongoose= require('mongoose');
const Schema=mongoose.Schema;

mongoose.Promises=global.Promise;

const insuranceSchema= new Schema({
    registration_number:String,
    insurance_number:String,
    owner_name:String,
    from_date: Date,
    end_date: Date,
    insurance_name:String
});

const Insurance   =   module.exports  =   mongoose.model("Insurance", insuranceSchema);

module.exports.addInsuranceDetails =   (newInsurance,callback)=>{
    Insurance.create(newInsurance,callback);
}


module.exports.updateInsurance   =   (updateInsurance,callback)=>{
    Insurance.findOne({registration_number:updateInsurance.registration_number},(err,insurance)=>{
        if(err){
            throw err;
        }
        else{
            id=insurance._id;
            Insurance.findByIdAndUpdate(id,updateInsurance,callback);
        }
    });
}
