const mongoose= require('mongoose');
const Schema=mongoose.Schema;

mongoose.Promises=global.Promise;

const vehiclercSchema=new Schema({
    registration_number:String,
    registration_date:Date,
    owner_name:String,
    chassis_number: String,
    vehicle_model: String,
    fuel_type: String
});

const Vehiclerc   =   module.exports  =   mongoose.model("Vehiclerc", vehiclercSchema);

module.exports.addVehiclerc =   (newVehicler,callback)=>{
    Vehiclerc.create(newVehicler,callback);
}

module.exports.updateVehiclerc  =   (registration_number,updateVehiclerc,callback)=>{
    Vehiclerc.findOne({registration_number:registration_number},(err,vehicle)=>{
        if(err){
            throw err;
        }
        else{
            id=vehicle._id;
            Vehiclerc.findByIdAndUpdate(id,updateVehiclerc,callback);
        }
    });
}


module.exports.removeVehiclerc  =   (registration_number,callback)=>{
    Vehiclerc.findOne({registration_number:registration_number},(err,vehicle)=>{
        if(err){
            throw err;
        }
        else{
            id=vehicle._id;
            Vehiclerc.findByIdAndRemove(id,callback);
        }
    });
}
