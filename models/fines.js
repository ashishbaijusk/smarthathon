const mongoose= require('mongoose');
const Schema=mongoose.Schema;

mongoose.Promises=global.Promise;

const fineSchema= new Schema({
    registration_number:String,
    fine_id:String,
    offense:String,
    fine_date:Date,
    fine_amount: Number,
    fine_status: String,
    police_username: String
});

const Fine   =   module.exports  =   mongoose.model("Fine", fineSchema);

module.exports.addFine   =   (newfine,callback)=>{
    Fine.create(newfine,callback);
}