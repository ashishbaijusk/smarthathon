const mongoose= require('mongoose');
const Schema=mongoose.Schema;

mongoose.Promises=global.Promise;

const logSchema= new Schema({
    log_registration_number:String,
    log_activity:String,
    log_date:Date
});

const Log   =   module.exports  =   mongoose.model("Log", logSchema);

module.exports.addLog   =   (newLog,callback)=>{
    Log.create(newLog,callback);
}
