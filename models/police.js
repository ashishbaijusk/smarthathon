const mongoose= require('mongoose');
const bcrypt = require('bcryptjs')
const Schema=mongoose.Schema;

mongoose.Promises=global.Promise;

const policeSchema=({
    police_name:String,
    police_id:String
});

const Police   =   module.exports  =   mongoose.model("Police", policeSchema);

module.exports.addPolice = (police_name, police_id, callback)=>{
    bcrypt.genSalt(10, (err,salt)=>{
        bcrypt.hash(police_id, salt, (err,hash)=>{
            if(err){
                throw err
            }
            else{
                police_id=hash
                Police.create({"police_name":police_name, "police_id":police_id},callback)
            }
        })
    })
}

module.exports.checkPolice  =  (police_id, hash, callback)=>{
    bcrypt.compare(police_id, hash, (err, isMatch)=>{
        if(err){
            throw err
        }
        else{
            callback(null, isMatch)
        }
    })
}