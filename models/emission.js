const mongoose= require('mongoose');
const Schema=mongoose.Schema;

mongoose.Promises=global.Promise;

const emissionSchema=new Schema({
    registration_number:String,
    number_plate_picture:String,
    start_date:Date,
    end_date:Date 
});

const Emission   =   module.exports  =   mongoose.model("Emission", emissionSchema);

module.exports.addEmissionDetails =   (newEmission,callback)=>{
    Emission.create(newEmission,callback);
}


module.exports.updateEmission   =   (updateEmission,callback)=>{
    Emission.findOne({registration_number:updateEmission.registration_number},(err,emission)=>{
        if(err){
            throw err;
        }
        else{
            id=emission._id;
            Emission.findByIdAndUpdate(id,updateEmission,callback);
        }
    });
}

